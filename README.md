# OAuth2_in_action


My notes about the [book](https://www.manning.com/books/oauth-2-in-action) oAuth2 in action 


## Chapter 1
## Chapter 2
### Getting and using tokens
* Issuing a token
* Using a token

### OAuth2 authorization grant
The authorization code uses a temporary credentials, the authorization code, to represent the resource owner authorization delegation.
1 : Resource Owner ask the client to accomplish a task in its behalf
2 : the client redirect the RO to the authorization server
3 : the RO authenticate itself to the AS (on the authorization endpoint) with information about the client (type, scope,...) as query parameters
4 : the RO authorize the client
5 : the AS redirects back to the client with the authorization code
6 : the client authenticates itself to the AS (on the token endpoint) and gives the authorization code to the AS
7 : the AS returns an access token to the client to access the protected resource. (here, a bearer token)
### OAuth actors
Clients : attempt to access protected resources (on behalf of someone)
Protected resource : needs a token to be accessed
Resource owner : usually the person using the client soft to access something they control
Authorization servers : central component of OAuth (authenticate RO, Client, delivers tokens. sometimes even performs token introspection and remembering authorization decisions)
### OAuth components
Tokens, scopes and authorization grant
#### Access tokens
Composed of : client's request access, the RO that authorized the client, the right confered by that authorization.
* Tokens are opaque to the client which just carries it.
* AS and PR can understand the token (to generate and validate it)
#### Scopes
It is a set of rights represented by strings, combined into a set by using a space separated list (->no space inside a scope string). The rest is undefined by OAuth.
Scopes are generally additive in nature.
#### Refresh tokens
Can be used by the client (sent to the AS) to request new access token to the PR.
The refresh token is *never* sent to the PR
This limits the exposure of the access token and the refresh token in separate but complementary ways (it exists in order to replace the long lived token from OAuth1 which had to be explicitly revoked)
#### Authorization grants
The entire OAuth process is the *authorization grant* : the client sending the user to the authorization endpoint, then receiving the code, then finally trading the code for the token.
There are different type of Authorization grants (authorization code in our examples)
### Interactions between actors and components
Back channel, front channel and endpoints
#### Back-Channel communication
Direct communications between :
* Client and AS : The Client (form encoded set of parameters) and the AS (Token endpoint, responds with a JSON object, the token)
* The Client and the PR
Happens in the *back channel* (aka not through your browser)
#### Front-channel communication
Indirect communication (through browser)
Front-channel communication is a method of using HTTP requests to communicate indirectly between two systems through an intermediary web browser.
Front-channel communication works by attaching parameters to a URL and indicating that the browser should follow that URL.
The two parties are thus communicating with each other indirectly through the use of the web browser as an intermediary.
(from the Client) The target of this redirect is the servers's URL with certain fields attached to it as query parameters
All information passed through the front channel is accessible to the browser (R/W)
## Chapter 3 : Building a simple OAuth client
### Register an OAuth client with an authorization server
The client identifier (client_id) must be unique for each client at a given authorization server, and is therefore almost always assigned by the authorization server to the client.
